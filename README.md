# Appunti Algoritmi e Strutture Dati

Questi sono i miei appunti di algoritmi e strutture dati scritti interamente in tempo reale durante le lezioni frontali dell'A.A. 2018/2019.

Gli appunti riguardano il corso di Algoritmi e Strutture Dati della facoltà di Informatica della [Università degli Studi di Napoli "Federico II"](https://www.unina.it).

## Nota importante

Sfortunatamente, non ho mai trovato l'occasione di integrare gli appunti propriamente con le videolezioni (o altre
registrazioni) dello stesso anno scolastico, pertanto ci sono svariate problematiche:

- Manca un filo logico e una coesione in alcune spiegazioni, dovute al fatto che cercavo di formalizzare un linguaggio parlato che non sempre ha un risultato ottimale.
- E' capitato diverse volte che mi assentassi durante le lezioni o che arrivassi in ritardo, quindi alcuni argomenti potrebbero mancare o essere incompleti. In genere, tali occorrenze sono annotate.
- Ci sono un po' di easter egg sparse che potrebbero non instillare molta fiducia.
- Imparavo nuove funzionalità di LaTeX man mano che scrivevo gli appunti -- la qualità, quindi, non è costante.
- La parte iniziale sulle notazioni asintotiche è assente.
- Gli argomenti non sono ordinati dal punto di vista "logico", sono ordinati seguendo l'ordine scelto dal professore durante la spiegazione.

Tra ogni lezione dovrebbe essere presente un indicatore con la data del giorno. Nel caso sia necessario, dovrebbe quindi risultare semplice integrare ciò che manca con il resto che è
stato spiegato.

Viste le considerazioni, consiglio _caldamente_ di utilizzare e integrare questi appunti con gli altri appunti noti, che sono di
qualità superiore (oltre ad essere scritti offline con più cura, calma e dedizione).
